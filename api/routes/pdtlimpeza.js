const express = require("express")
const router = express.Router()
const pdt = require("../model/pdtlimpeza")

var searchFilter = function(obj, filter){
    if(obj.classificacao == filter){
        return obj
    }
}

router.get("/", async function(req, res){
    let response = await pdt.find().limit(parseInt(req.query.limit) || 5)

    if(!req.query.filter){
        res.json(response)
    }
    else{
        res.json(response.filter(searchFilter, req.query.filter))
    }
})

router.get("/:classificacao", async function(req, res){
    res.json(pdt.findOne({"classificacao": req.params.classificacao}))
})

router.post("/", async function (req, res){
    
})