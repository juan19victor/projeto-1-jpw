const mongoose = require("mongoose")

let schema = mongoose.Schema({
    nome: String,
    lote: String,
    datav: String
})

let ingrediente = mongoose.model("ingrediente", schema)

module.exports = ingrediente