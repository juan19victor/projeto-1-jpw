const mongoose = require("mongoose")

let schema = mongoose.Schema({
    sabor: String,
    datap: String,
    datav: String,
    lote: String
})

let kombucha = mongoose.model("kombucha", schema)

module.exports = kombucha