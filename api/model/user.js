const mongoose = require("mongoose")

let schema = new mongoose.Schema({
    authKey: Number,
    cadastro: {
        nome: String,
        endereco: {
            rua: String,
            numero: Int8Array,
            cidade: String,
            estado: String
        }
    }
})

let user = mongoose.model("user", schema)

module.exports = user